<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ComplaintController as AdminComplaintController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Student\ComplaintController as StudentComplaintController;
use App\Http\Controllers\Student\ResponseController as StudentResponseController;
use App\Http\Controllers\Student\UserController as StudentUserController;
use App\Http\Controllers\Student\StudentController as StudentStudentController;
use App\Http\Controllers\Operator\OperatorController as OperatorOperatorController;
use App\Http\Controllers\Operator\ResponseController as OperatorResponseController;
use App\Http\Controllers\Operator\StudentController as OperatorStudentController;
use App\Http\Controllers\Operator\UserController as OperatorUsertController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {
  Route::middleware(['level:admin'])->group(function () {
    Route::get('/admin', function () {
      return view('admin.home');
    });
    Route::resource('/admin/users', AdminUserController::class);
    Route::resource('/admin/students', AdminStudentController::class);
    Route::resource('/admin/complaints', AdminComplaintController::class);
    Route::resource('/admin/responses', AdminResponseController::class);
    Route::get('/admin/complaint-report', [AdminSummaryController::class, 'complaintReport']);
  });

  Route::get('/operator', function () {
    return view('operator.home');
  });
  Route::resource('/operator/responses', OperatorResponseController::class);
  Route::resource('/operator/students',  OperatorStudentsController::class);
  Route::resource('/operator/users',     OperatorUserController::class);

  Route::middleware(['level:student'])->group(function () {
    Route::get('/student', function () {
      return view('student.home');
    });
    Route::resource('/student/complaints', StudentComplaintController::class);
    Route::resource('/student/responses', StudentResponseController::class);
    Route::resource('/student/users', StudentUsersController::class);
    Route::resource('/student/students', StudentStudentsController::class);
    Route::resource('/student/operators', StudentOperatorController::class);
  });
  
});
Route::get('/kontak', function () {
  return view('kontak');
});
Route::get('/About', function () {
  return view('About');
});
Route::get('/lapor', function () {
  return view('lapor');
});
Route::get('/home', function () {
  return view('home');
});
Route::get('/Galeri', function () {
  return view('Galeri');
});

Route::get('/login', [LoginController::class, 'show'])->name('login');
  Route::post('/login', [LoginController::class, 'check']);
  Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/complaints.index', [ComplaintController::class, 'index'])->name('index');
