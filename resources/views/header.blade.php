<?php
$level = auth()->user()->level;
?>
<header class="p-4 mb-5 border-bottom" id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none">
            <img src="/img/images.jpeg" style="width:100px">
            <span class="f-4 ms-2">LPG</span> 
            <div class="col-3 mb-4"></div>
            <p> Laporan Perundungan Gracia</p>
</a>
              <br><br><br>
         <ul class="nav me-auto">
            <li><a href="home" class="nav-link">Home</a></li>
            <li><a href="About" class="nav-link">About</a></li>
            <li><a href="Galeri" class="nav-link">Galeri</a></li>
            <li><a href="/lapor" class="nav-link">Lapor</a></li>
            <li><a href="kontak" class="nav-link">Kontak</a></li>
        </ul>
            <ul class="nav flex-coloumn mb-auto">
            @if ($level != 'student')
                <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
            @endif    
                <li><a href="/{{ $level }}/complaints" class="nav-link">Complaint</a></li>
                <li><a href="/logout" class="dropdown-item">Log out</a></li>
        </ul>
        </ul>
    </div>
</header>