<section class="section-footer">
  <Footer>
  <footer class="site-footer" style="width: 17rem">
    <div class="container">
      <div class="row row-sub-footer">
        <div class="col-12 col-md-6 col-lg-3">
          <div class="wrapper-col-1 mb-5"></div>
          <h1>SMK GRACIA BANDUNG</h1>
					<span>Jl. Cibadak Gg. Sereh, no. 26 Kec. Astanaanyar Kota Bandung, Jawa Barat</span>
          <br>
          <i class="fa fa-phone"></i>
          <span>022-6014810</span>
        </div>
        <div class="col-12 col-mb-12">
            <h6>ABOUT SMK GRACIA</h6>
            <span>Sekolah Gracia adalah sekolah umum yang memiliki keanekaragaman dalam memberikan pendidikan agama sehingga terdapat Agama Islam, Kristen Protestan, Kristen Khatolik dan Budha dari tingkat TK sampai dengan tingkat SMK</span>
        </div>
      </div>
    </div>
        <div class="col-12 col-md-6 col-lg-3 mb-5">
        <div class="contact">
          <div class="wrapper-col-6">
            <h1>Contact Info</h1>
            <a href="#">Sekolahgracia@gmail.com</a>
            <a href="#">+022 6014810 </a>
            <a href="#">Kota Bandung, Jawa Barat</a>
          </div>
      </div>
      </div>
    

      <div class="wrapper-last-footer d-flex justify-content-between">
        <div class="copyright">@2023 - All right reserved</div>
      </div>
    </div>
  </footer>
</section>