@extends('app')

@section('content')
<div class="container">
  <h1>Detail User</h1>
  <form action="/admin/users/{{ $user->id }}" method="POST">
  </form>
  @if ($errors->any())
  @foreach ($errors->all () as $error)
  <p class="text-danger">{{ $error }}</p>
  @endforeach
  @endif

  @crsf
  @method('PATCH')
  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="name" class="form-label">name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $user->name}}" disabled>
    </div>

  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="gender" class="form-label">gender</label>
      <input type="text" class="form-control" id="gender" name="gender" value="{{ $user->gender}}" disabled>
    </div>

    <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="date_of_birth" class="form-label">date_of_birth</label>
        <input type="text" class="form-control" id="date_of_birth" name="date_of_birth"  value="{{ $user->date_of_birth}}" disabled>
      </div>

      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="username" class="form-label">username</label>
        <input type="text" class="form-control" id="username" name="username" value="{{ $user->username}}" disabled>
      </div>

      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="password" class="form-label">Password</label>
        <input type="text" class="form-control" id="password" name="password" value="{{ $user->password}}" disabled>
      </div>

      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="email" class="form-label">email</label>
        <input type="text" class="form-control" id="password" name="email" value="{{ $user->email}}" disabled>
      </div>

      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="phone" class="form-label">Phone</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone}}" disabled>
      </div>

      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="address" class="form-label">address</label>
        <input type="text" class="form-control" id="address" name="address" value="{{ $user->addres}}">
      </div>

      <label class="form-label">level</label>
      @foreach (['admin', 'operator', 'student'] as $item)
      <div class="form-check">
        <input class="form-check-input" type="radio" name="level" value="{{ $item }}">
        <input type="text" class="form-control" id="username" name="username" value="{{ $user->level}}"  disabled>
        {{ $user->level == $item ? 'checked' : '' }}>
        <label class="form-check-label">{{ $item }}</label>
      </div>
      @endforeach
    </div>