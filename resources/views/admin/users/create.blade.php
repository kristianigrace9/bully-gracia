@extends('app')

@section('content')
<div class="container">
    <h1>Detail User</h1>
    <form action="/admin/users" method="POST">
        @csrf
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="name" class="form-label">name</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="gender" class="form-label">gender</label>
                <input type="text" class="form-control" id="gender" name="gender">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="date_of_birth" class="form-label">date_of_birth</label>
                <input type="text" class="form-control" id="date_of_birth" name="date_of_birth">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="username" class="form-label">username</label>
                <input type="text" class="form-control" id="username" name="username">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="password" class="form-label">password</label>
                <input type="text" class="form-control" id="name" name="password">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="email" class="form-label">email</label>
                <input type="text" class="form-control" id="email" name="email">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="phone" class="form-label">phone</label>
                <input type="text" class="form-control" id="phone" name="phone">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="address" class="form-label">address</address></label>
                <input type="text" class="form-control" id="addres" name="address">
            </div>
        </div>


        <label class="form-label">level</label>
        @foreach (['admin', 'operator', 'student'] as $item)
        <div class="form-check">
            <input class="form-check-input" type="radio" name="level" value="{{ $item }}">
            <input type="text" class="form-control" id="username" name="username" disabled>
            <label class="form-check-label">{{ $item }}</label>
        </div>
        @endforeach

        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <p class="text-danger">{{ $error }}</p>
    @endforeach
    @endif
</div>
@endsection