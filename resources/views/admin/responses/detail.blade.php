@extends('app')

@section('content')
    <div class="container">
        <h1>Detail Response</h1>
        <form action="/admin/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">Response Date</label>
                    <input type="date" class="form-control" id="date" name="date">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
            </div>
            <div class="row flex-coloumn">
                <div class="col-3 mb-3">
                    <label for="complaint_id" class="form-label">Complaint id</label>
                    <input type="text" class="form-control" id="complaint_id" name="complaint_id">
                </div>
            </div>
           
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection

       