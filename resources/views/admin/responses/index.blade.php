@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $response_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>no</th>
                <th>response_date</th>
                <th>response</th>
                <th>complaint_id</th>
                <th>operator_id</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($response_list as $response)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $response->response_date }}</td>
                <td>{{ $response->response }}</td>
                <td>{{ $response->complaint_id }}</td>
                <td>{{ $response->operator_id}}</td>
                <td>
                <a href="/admin/esponses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                            <form action="/admin/response/{{ $response->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success">Tambah</a>
    </div>
@endsection