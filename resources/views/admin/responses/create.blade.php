@extends('app')

@section('content')
<div class="container">
    <h1>Detail responses</h1>
    <form action="/admin/responses" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="response_date" class="form-label">response_date</label>
                <input type="date" class="form-control" id="date" name="date">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="response" class="form-label">response</label>
                <input type="text" class="form-control" id="response" name="response">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="complaint_id" class="form-label">complaint_id</label>
                <input type="text" class="form-control" id="complaint_id" name="complaint_id" accept="image/png,image/jpeg">
            </div>
        </div>


        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <p class="text-danger">{{ $error }}</p>
    @endforeach
    @endif
</div>
@endsection