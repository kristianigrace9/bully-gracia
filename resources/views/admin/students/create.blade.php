@extends('app')

@section('content')
<div class="container">
    <h1>Detail Student</h1>
    <form action="/admin/students" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="nisn" class="form-label">nisn</label>
                <input type="date" class="form-control" id="nisn" name="nisn">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="class" class="form-label">class</label>
                <input type="text" class="form-control" id="class" name="class">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="user_id" class="form-label">User_id</label>
                <input type="text" class="form-control" id="user_id" name="user_id"
                    accept="image/png,image/jpeg">
            </div>
        </div>

        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="operator_id" class="form-label">operator_id</label>
                <input type="text" class="form-control" id="operator_id" name="operator_id">
            </div>
        </div>


        <div class="col-3 mb-3">
        <label class="form-label">user ID</label>
        <select name="user_id" class="form-select">
            @foreach ($user_list as $user)
               <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->class }}</option>
               @endforeach
        </select>
      </div>

        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <p class="text-danger">{{ $error }}</p>
    @endforeach
    @endif
</div>
@endsection