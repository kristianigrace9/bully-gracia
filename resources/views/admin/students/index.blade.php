@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $complaint_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>no</th>
                <th>nisn</th>
                <th>clas</th>
                <th>user_id</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($complaint_list as $complaint)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $student->nisn }}</td>
                <td>{{ $student->class }}</td>
                <td>{{ $student->user_id }}</td>
                <td>
                <a href="/admin/complaints/" class="btn-btn-primary">Detail</a>
                    <form action="/admin/complaints/">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success">Tambah</a>
    </div>
@endsection