@extends('app')

<div class="container">
  <h1>Detail student</h1>
  <form action="admin/students"{{ $student->id }}" method="POST">
  </form>
  @if ($errors->any())
  @foreach ($errors->all () as $error)
  <p class="text-danger">{{ $error }}</p>
  @endforeach
  @endif

  @crsf
  @method('PATCH')
  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="nisn" class="form-label">nisn</label>
      <input type="text" class="form-control" id="nisn" name="nisn">
    </div>

  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="class" class="form-label">class</label>
      <input type="text" class="form-control" id="@extends('app')

@section('content')
<div class="container">
  <h1>Detail student</h1>
  <form action="/admin/student{{ $user->id }}" method="POST">
  </form>
  @if ($errors->any())
  @foreach ($errors->all () as $error)
  <p class="text-danger">{{ $error }}</p>
  @endforeach
  @endif

  @crsf
  @method('PATCH')
  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="complaint_id" class="form-label">complaint_id</label>
      <input type="text" class="form-control" id="complaint_id" name="complaint_id">
    </div>

  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="content" class="form-label">content</label>
      <input type="text" class="form-control" id="content" name="content">
    </div>

    <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="photo" class="form-label">photo</label>
        <input type="text" class="form-control" id="photo" name="photo">
      </div>



      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="operator_id" class="form-label">operator_id</label>
        <input type="text" class="form-control" id="operator_id" name="operator_id">
      </div>


    <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="user_id" class="form-label">user_id</label>
        <input type="text" class="form-control" id="user_id" name="user_id">
      </div>


      <label class="form-label">level</label>
      @foreach (['admin', 'operator', 'student'] as $item)
      <div class="form-check">
        <input class="form-check-input" type="radio" name="level" value="{{ $item }}">
        <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }} " disabled>
        {{ $user->level == $item ? 'checked' : '' }}>
        <label class="form-check-label">{{ $item }}</label>
      </div>
      @endforeach
    </div>