@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $response_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>no</th>
                <th>response_date</th>
                <th>response</th>
                <th>complaint_id</th>
                <th>operator_id</th>
                <th></th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($operator_list as $operator)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $operator->response_date }}</td>
                <td>{{ $operator->response }}</td>
                <td>{{ $operator->complaint_id }}</td>
                <td>{{ $operator->operator_id}}</td>
                <td>{{ $operator->student->name}}</td>
                <td>{{ $operator->student->user}}</td>

                <td>
                    <a href="#" class="btn-btn-info">Detail</a>
                    <a href="#" class="btn-btn-danger">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection