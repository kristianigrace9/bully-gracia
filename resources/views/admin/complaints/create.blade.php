@extends('app')

@section('content')
<div class="container">
    <h1>Detail Complaint</h1>
    <form action="/admin/complaints" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="complaint_date" class="form-label">complaint_date</label>
                <input type="date" class="form-control" id="complaint_date" name="complaint_date">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="content" class="form-label">content</label>
                <input type="text" class="form-control" id="content" name="content">
            </div>
        </div>
        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="photo" class="form-label">photo</label>
                <input type="text" class="form-control" id="photo" name="photo"
                    accept="image/png,image/jpeg">
            </div>
        </div>

        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="status" class="form-label">status</label>
                <input type="text" class="form-control" id="status" name="status"
                    accept="image/png,image/jpeg">
            </div>
        </div>

        <div class="row flex-column">
            <div class="col-3 mb-3">
                <label for="operator_id" class="form-label">operator_id</label>
                <input type="text" class="form-control" id="operator_id" name="operator_id">
            </div>
        </div>


        <div class="col-3 mb-3">
        <label class="form-label">user ID</label>
        <select name="user_id" class="form-select">
            @foreach ($user_list as $user)
               <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->class }}</option>
               @endforeach
        </select>
      </div>

        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <p class="text-danger">{{ $error }}</p>
    @endforeach
    @endif
</div>
@endsection