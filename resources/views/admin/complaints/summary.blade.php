@extends('app')

@section('content')
    <div class="container">
        <h1>Laporan Pengaduan Bullying</h1>
        <form action="/admin/complaint-report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">                    
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                <th>no</th>
                <th>complaint_date</th>
                <th>content</th>
                <th>photo</th>
                <th>user_id</th>
                <th>operator_id</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($complaint_list as $complaint)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $complaint->complaint_date }}</td>
                <td>{{ $complaint->content }}</td>
                <td>{{ $complaint->photo }}</td>
                <td>{{ $complaint->user_id }}</td>
                <td>{{ $complaint->operator_id}}</td>
                        <td>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $complaint->id }}" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div> 
                                <div class="modal-body">
                                    <p>Complaint dengan ID {{ $complaint->id }} akan dihapus.</p>
                                    <p>Lanjutkan?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" onclick="window.print()">
            Print
        </button>
    </div>
@endsection
 