@extends('app')

@section('content')
    <div class="halaman-lapor"></div>
    <div class="container">
        <div class="row">
            <div class="col-6 mx-auto">
                <h1>Laporan Pengaduan</h1>
                <form action="/student/complaints" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="complaint_date" class="form-label">Complaint_date</label>
                            <input type="date" class="form-control" id="complaint_date" name="complaint_date">
                        </div>
                    </div>
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="content" class="form-label"> Content</label>
                            <input type="text" class="form-control" id="content" name="content">
                        </div>
                    </div>
                    <div class="row flex-coloumn">
                        <div class="col-12 mb-3">
                            <label for="photo" class="form-label">Bukti</label>
                            <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </form>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach
                @endif
            </div>
        </div>
        
    </div>
@endsection