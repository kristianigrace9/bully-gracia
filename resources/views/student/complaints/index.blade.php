@extends('app')

@section('content')
<div class="container">
    <h1></h1>
    <p>{{ $complaint_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>no</th>
                <th>complaint_date</th>
                <th>content</th>
                <th>photo</th>
                <th>user_id</th>
                <th>operator_id</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($complaint_list as $complaint)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $complaint->complaint_date }}</td>
                <td>{{ $complaint->content }}</td>
                <td>{{ $complaint->photo }}</td>
                <td>{{ $complaint->user_id }}</td>
                <td>{{ $complaint->operator_id}}</td>
                <td>
                <a href="/student/complaint/{{ $complaint->id }}" class="btn btn-primary">Detail</a>
                            <form action="/student/complaint/{{ $complaint->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success">Tambah</a>
    </div>
@endsection