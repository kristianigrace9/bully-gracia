@extends('app')

@section('content')
<div class="container">
  <h1>Detail complaint</h1>
  <form action="student/complaint//{{ $complaint->id }}" method="POST">
  </form>
  @if ($errors->any())
  @foreach ($errors->all () as $error)
  <p class="text-danger">{{ $error }}</p>
  @endforeach
  @endif

  @crsf
  @method('PATCH')
  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="complaint_id" class="form-label">complaint_id</label>
      <input type="text" class="form-control" id="complaint_id" name="complaint_id">
    </div>

  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="content" class="form-label">content</label>
      <input type="text" class="form-control" id="@extends('app')

@section('content')
<div class="container">
  <h1>Detail User</h1>
  <form action="/student/users/{{ $user->id }}" method="POST">
  </form>
  @if ($errors->any())
  @foreach ($errors->all () as $error)
  <p class="text-danger">{{ $error }}</p>
  @endforeach
  @endif

  @crsf
  @method('PATCH')
  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="complaint_id" class="form-label">complaint_id</label>
      <input type="text" class="form-control" id="complaint_id" name="complaint_id">
    </div>

  <div class="row flex-column">
    <div class="col-3 mb-3">
      <label for="content" class="form-label">content</label>
      <input type="text" class="form-control" id="content" name="content">
    </div>

    <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="photo" class="form-label">photo</label>
        <input type="text" class="form-control" id="photo" name="photo">
      </div>



      <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="operator_id" class="form-label">operator_id</label>
        <input type="text" class="form-control" id="text" name="text">
      </div>


    <div class="row flex-column">
      <div class="col-3 mb-3">
        <label for="user_id" class="form-label">user_id</label>
        <input type="text" class="form-control" id="text" name="text">
      </div>


      <label class="form-label">level</label>
      @foreach (['admin', 'operator', 'student'] as $item)
      <div class="form-check">
        <input class="form-check-input" type="radio" name="level" value="{{ $item }}">
        <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }} " disabled>
        {{ $user->level == $item ? 'checked' : '' }}>
        <label class="form-check-label">{{ $item }}</label>
      </div>
      @endforeach
    </div>