@extends('app')

@section('content')
    <div class="container">
        <h1>Tanggapan</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>operator_id</th>
                    <th>content</th>
                    <th>response</th>
                    <th>response date</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    @if ($response->complaint->user->id == auth()->user()->id)
                        <tr>
                            
                            <td>{{ $response->operator_id }}</td>
                            <td>{{ $response->complaint->content }}</td>
                            <td>{{ $response->response }}</td>
                            <td>{{ $response->response_date }}</td>
                            <td>
                                <a href="/student/response/{{ $response->id }}" class="btn btn-warning">Detail</a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
