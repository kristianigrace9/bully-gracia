<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gender',
        'date_of_birth',
        'username',
        'password',
        'email',
        'phone',
        'address',
        'created_at',
        'updated_at',
    ];
    public function complaints() {
        return $this->hasMany(complaint::class, 'user_id');
    }
}