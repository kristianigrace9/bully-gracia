<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;
    protected $fillable = [
        'complaint_date',
        'content',
        'photo',
        'status',
        'user_id',
        'created_at',
        'updated_at'
    ];
}
