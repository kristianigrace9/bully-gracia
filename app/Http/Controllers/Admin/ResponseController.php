<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaint;
use Illuminate\Http\Request;
use App\Models\Response;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Response::paginate(5);
        return view('admin.responses.index', ['response_list'=> $response]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responses = Response::all();
        $complaints = Complaint::all();
        return view('admin.responses.create', ['complaint_list' => $complaints], ['response_list' => $responses]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'complaint_id' => 'required',
            'response_date' => 'required',
            'response' => 'required',
        ]);

        $data['operator_id'] = Auth::user()->id;

        Response::create($data);

        return redirect('/admin/responses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
        'response_date' => 'required',
        'response' => 'required',
        'complaint_id' => 'required',
        'operator_id' => 'required',
        'proof_file' => 'nullable|file'
        ]);

        if (array_key_exists('proof_file', $data)) {
            $path = $request->proof_file->store('public/images');
            $data['proof_file'] = str_replace('public/',  '', $path);
        }

          response::where('id', $id)->update($data);

          return redirect('/admin/responses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
