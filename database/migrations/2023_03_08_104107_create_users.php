<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->enum('gender', ['female', 'male']);
            $table->string('date_of_birth', 25);
            $table->string('username', 25)->unique();
            $table->string('password', 255);
            $table->string('email', 50)->unique();
            $table->string('phone', 14);
            $table->string('address', 100);
            $table->enum('level',['admin', 'operator', 'student'])->default('student');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
