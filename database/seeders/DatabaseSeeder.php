<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = User::create([
            'name' => 'marshena andrea',
            'gender' => 'female',
            'date_of_birth' => '2004-11-30',
            'username' => 'marshena',
            'password' => Hash::make('Coba_234'),
            'email' => 'marshena18@gmail.com',
            'phone' => '08774564333',
            'address' => 'Jalan cibuntu no.13',
            'level' => 'student'
        ]);
        $user_2 = User::create([
            'name' => 'grace angelina',
            'gender' => 'female',
            'date_of_birth' => '2003-07-20',
            'username' => 'grace',
            'password' => Hash::make('coba_125'),
            'email' => 'angelinagrace@gmail.com',
            'phone' => '081220637880',
            'address' => 'Jalan kopo raya no.13',
            'level' => 'student'
        ]);
         $user_3 = User::create([
            'name' => 'amelia venesa',
            'gender' => 'female',
            'date_of_birth' => '2005-08-10',
            'username' => 'amelia',
            'password' => Hash::make('Coba_080625'),
            'email' => 'ameliavenesa0806@gmail.com',
            'phone' => '0895356012317',
            'address' => 'Jalan cibangkong no.90',
            'level' => 'admin'
        ]);
        $user_4 = User::create([
            'name' => 'christian andre',
            'gender' => 'male',
            'date_of_birth' => '2003-11-08',
            'username' => 'christian',
            'password' => Hash::make('ayo_144'),
            'email' => 'christian@gmail.com',
            'phone' => '08122065470',
            'address' => 'Jalan bandung raya n0.14',
            'level' => 'admin'
        ]);
        $user_5 = User::create([
            'name' => 'catherine cyintia',
            'gender' => 'female',
            'date_of_birth' => '2002-12-13',
            'username' => 'catherine',
            'password' => Hash::make('Coba_225'),
            'email' => 'catherine14@gmail.com',
            'phone' => '081920906424',
            'address' => 'Jalan cimahi no.34',
            'level' => 'operator'
        ]);
        $user_6 = User::create([
            'name' => 'karin divana tania',
            'gender' => 'female',
            'date_of_birth' => '2004-11-12',
            'username' => 'karin',
            'password' => Hash::make('kue_863'),
            'email' => 'divanakarin12@gmail.com',
            'phone' => '0812210135',
            'address' => 'Jalan rahayu no.59',
            'level' => 'operator'
        ]);


        $student_1 = Student::create([
            'nisn' => '00534499212',
            'class' => '12',
            'user_id' => $user_1->id
        ]);
        $student_2 = Student::create([
            'nisn' => '00537892167',
            'class' => '12',
            'user_id' => $user_2->id
        ]);

        $complaint_1 = Complaint::create([
            'complaint_date' => '2022-12-12',
            'content' => 'saya mengalami ejekan',
            'photo' => 'bukti',
            'status' => 'new',
            'user_id' => $user_1->id,
        ]);
        $complaint_2 = Complaint::create([
            'complaint_date' => '2023-01-22',
            'content' => 'saya mengalami ejekan',
            'photo' => 'Bukti',
            'status' => 'verified',
            'user_id' => $user_2->id,
        ]);
        $response_1 = Response::create([
            'response_date' => '2022-12-15',
            'response' => 'Kami akan memberikan surat peringaratan',
            'complaint_id' => $complaint_1->id,
            'operator_id' => $user_2->id
        ]);
        $response_2 = Response::create([
            'response_date' => '2022-02-02',
            'response' => 'Kami akan memberikan surat peringaratan',
            'complaint_id' => $complaint_2->id,
            'operator_id' => $user_3->id
        ]);
    }
}
